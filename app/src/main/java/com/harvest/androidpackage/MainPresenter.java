package com.harvest.androidpackage;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

/**
 * Created by jimmy.lee
 */
public class MainPresenter extends MvpBasePresenter<MainView> {
    private static final String tag = MainPresenter.class.getSimpleName();

    public void initialize() {

    }
}
