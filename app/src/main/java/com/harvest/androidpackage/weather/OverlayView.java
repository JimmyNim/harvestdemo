package com.harvest.androidpackage.weather;


import com.harvest.androidpackage.common.PreferenceManager;

/**
 *
 */
public interface OverlayView {

    public void updateWeatherInfo(PreferenceManager.Weather item);

    public void updateDateNTime();

    public interface OverlatListener {
        public enum OverlayEvent {
            OnDemand(0),
            LiveTV(1),
            CatchUp(2),
            Guide(3),
            HDVoice(4),
            Preferences(5);
            private int mIndex;
            OverlayEvent(int index) {
                this.mIndex = index;
            }
            public int getIndex() {
                return mIndex;
            }
        }
        public void onClick(OverlayEvent event);
    }
}
