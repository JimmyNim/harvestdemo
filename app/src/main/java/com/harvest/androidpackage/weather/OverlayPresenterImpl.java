package com.harvest.androidpackage.weather;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;

import com.harvest.androidpackage.common.IMainThread;
import com.harvest.androidpackage.common.MainThreadImpl;
import com.harvest.androidpackage.common.PreferenceManager;
import com.harvest.tvs.util.CalendarManager;
import com.harvest.tvs.util.Log;
import com.harvest.tvs.yahoo.WeatherManager;

import java.util.Calendar;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class OverlayPresenterImpl implements OverlayPresenter {
    private static final String tag = OverlayPresenterImpl.class.getSimpleName();
    private Subscription mSubscription;

    Context context;
    OverlayView view;
    private OverlayPresenterImpl(Context context, OverlayView view) {
        this.context = context;
        this.view = view;
//        init();
        initSingleWeatherInfo();
    }


    public static OverlayPresenterImpl newInstance(Context context, OverlayView view) {
        OverlayPresenterImpl presenter = new OverlayPresenterImpl(context, view);
        return presenter;
    }

    @Override
    public void onResume() {
        Log.d(tag, "onResume..");
        mLoopHandler.post(mWeatherRunnable);
        registerReceiver();
    }

    @Override
    public void onStop() {
        Log.d(tag, "onStop..");
        mLoopHandler.removeCallbacks(mWeatherRunnable);
        unregisterReceiver();
        releaseRxTask();
    }

    @Override
    public String getTimeFormatter(long timeMillis) {
        return CalendarManager.getTimeFormatter(timeMillis);
    }
    @Override
    public String getDateFormatter(long timeMillis) {
        return CalendarManager.getDateFormatter(timeMillis);
    }

    public void releaseRxTask() {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    private IMainThread mainThread;
    private Runnable mWeatherRunnable;
    private int WeatherIndex = 0;
    private Handler mLoopHandler = new Handler();
    private static final int INTERVAL_TIME_30MS	    = 30000;
    private OverlayPresenter presenter;
    private void init() {
        mainThread = new MainThreadImpl();
        mWeatherRunnable = () -> {
            synchronized (this) {
                if(isSavedWeatherInfo()) {
                    // Timmer 30second refresh next city.
                    int woeid = WeatherManager.INSTANCE.getForcastWoeIdInSystemTimeZone();
                    PreferenceManager.Weather weather = PreferenceManager.INSTANCE.getWeatherInfo(woeid);
                    if(weather!=null) {
                        mainThread.post(() -> {
                            view.updateWeatherInfo(weather);
                            WeatherIndex++;
                            mLoopHandler.postDelayed(mWeatherRunnable, INTERVAL_TIME_30MS);
//                            Log.d(tag, String.format("WeatherMain(10)[%s][%s]", "Finish!!!", ""));
                        });
                    }
                } else {
                    mSubscription = WeatherManager.INSTANCE.getWeatherServiceByGMT()
                            .subscribeOn(Schedulers.io())
                            .subscribe(weatherItem -> {
                                Log.d(tag, String.format("subscribe(1)[%s][%s]", "" + weatherItem.toString(), ""));
                                PreferenceManager.INSTANCE.setWeatherInfo(weatherItem);
                            } , throwable -> {
                                throwable.printStackTrace();
                                mLoopHandler.postDelayed(mWeatherRunnable, 3000);
                            } , () -> {
                                mLoopHandler.postDelayed(mWeatherRunnable, 100);
                                Log.d(tag, String.format("completed(3)[%s][%s]", "Finish!!!", ""));
                            });
                }
            }
        };
    }

    private void initSingleWeatherInfo() {
        if(isSavedWeatherInfo()) {
            int woeid = WeatherManager.INSTANCE.getForcastWoeIdInSystemTimeZone();
            PreferenceManager.Weather weather = PreferenceManager.INSTANCE.getWeatherInfo(woeid);
            if(weather!=null) {
                view.updateWeatherInfo(weather);
            } else {
                loadWeatherInfo();
            }
        } else {
            loadWeatherInfo();
        }
    }

    private void loadWeatherInfo() {
        mSubscription = WeatherManager.INSTANCE.getWeatherServiceByGMT()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(weatherItem -> {
                    Log.d(tag, String.format("subscribe(1)[%s][%s]", "" + weatherItem.toString(), ""));
                    PreferenceManager.INSTANCE.setWeatherInfo(weatherItem);
                    PreferenceManager.Weather weather = PreferenceManager.INSTANCE.getWeatherInfo(weatherItem.woeid);
                    if(weather!=null) {
                        view.updateWeatherInfo(weather);
                    }
                } , throwable -> {
                    throwable.printStackTrace();
                } , () -> {
                    Log.d(tag, String.format("completed(3)[%s][%s]", "Finish!!!", ""));
                });
    }

    private boolean isSavedWeatherInfo() {
        Calendar currentCal = Calendar.getInstance();
        Calendar savedCal = Calendar.getInstance();
        long savedTime = PreferenceManager.INSTANCE.getSavedTimeMillisOfWeather();
        savedCal.setTimeInMillis(savedTime);
        savedCal.add(Calendar.HOUR, 3);
//        savedCal.add(Calendar.MINUTE, 30);
        if(currentCal.getTimeInMillis() < savedCal.getTimeInMillis()) {
            return true;
        }
        return false;
    }

    /*
	 *********************************************************************
	 * Update : date and time
	 **********************************************************************
	 */
    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        context.registerReceiver(receiver, filter);
    }

    private void unregisterReceiver() {
        context.unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Intent.ACTION_TIME_TICK.equals(action)) {
                Log.d(tag, String.format("onReceive()[%s][%s]", "", "" + action));
                view.updateDateNTime();
            }
        }
    };

}
