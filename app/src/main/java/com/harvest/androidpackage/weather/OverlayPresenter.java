package com.harvest.androidpackage.weather;

/**
 *
 */
public interface OverlayPresenter {
    public void onResume();

    public void onStop();

    public String getTimeFormatter(long timeMillis);

    public String getDateFormatter(long timeMillis);

}
