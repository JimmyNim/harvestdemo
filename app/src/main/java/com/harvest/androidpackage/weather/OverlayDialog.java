package com.harvest.androidpackage.weather;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.harvest.androidpackage.R;
import com.harvest.androidpackage.common.PreferenceManager;
import com.harvest.tvs.util.Log;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 *
 */
public class OverlayDialog extends DialogFragment implements OverlayView {
    private static final String tag = OverlayDialog.class.getSimpleName();

	OverlayView.OverlatListener listener;
	public void setOnDialogClickListener(OverlayView.OverlatListener l) {
		this.listener = l;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.OverlayDialog);
	}

	@BindView(R.id.Overlay_Index01)
	TextView mTime;
	@BindView(R.id.Overlay_Index02)
	TextView mDate;
	@BindView(R.id.Overlay_Index03)
	TextView mWeatherInfo;
	@BindView(R.id.Overlay_Index04)
	TextView mWeatherICON;
	Unbinder mUnbinder;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	Log.d(tag, "onCreateView()");
    	super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.cstdialog_overlay, container);
		mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Log.d(tag, "onViewCreated()");
		init();
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(tag, "onResume..");
		presenter.onResume();
	}
    
    @Override
	public void onStop() {
		super.onStop();
		Log.d(tag, "onStop..");
		presenter.onStop();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mUnbinder.unbind();
	}


	OverlayPresenter presenter;
	private void init() {
		presenter = OverlayPresenterImpl.newInstance(getActivity(), this);
		updateDateNTime();
	}

	@Override
	public void updateWeatherInfo(PreferenceManager.Weather item) {
		Log.d(tag, String.format("%s %sºC", "" + item.city, "" + item.temperature));
		if (mWeatherInfo != null) {
			mWeatherInfo.setText(String.format("%s %sºC", "" + item.city, "" + item.temperature));
		}
        if (mWeatherICON != null){
            mWeatherICON.setBackgroundResource(0);
            int code = Integer.valueOf(item.temperature_code);
            if(code!=0) {
                String resName = getWeatherResourceFileName(code);
                int resId = getResources().getIdentifier(resName, "drawable",
                        getActivity().getPackageName());
                mWeatherICON.setBackgroundResource(resId);
            }
        }
	}

	private final String PREPIX_Weather_ = "weather_";
	private String getWeatherResourceFileName(int offset) {
		StringBuilder sb = new StringBuilder();
		sb.append(PREPIX_Weather_);
		if (offset < 10) {
			sb.append("0" + offset);
		} else {
			sb.append(offset);
		}
		Log.d(tag, String.format("getWeatherResourceFileName()[%s][%s]",
				"" + offset, "" + sb.toString()));
		return sb.toString();
	}

	public void updateDateNTime() {
		long currentTimeMillis = Calendar.getInstance().getTimeInMillis();
		mTime.setText(presenter.getTimeFormatter(currentTimeMillis));
		mDate.setText(presenter.getDateFormatter(currentTimeMillis));
	}
}
