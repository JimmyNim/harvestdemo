package com.harvest.androidpackage.rolling;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.harvest.androidpackage.R;
import com.harvest.androidpackage.common.FragmentLoader;
import com.harvest.androidpackage.common.KeyDispatchable;
import com.harvest.androidpackage.rolling.view.HorizontalRollingContainer;
import com.harvest.tvs.util.Log;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FragmentRolling extends MvpFragment<RollingView, RollingPresenter> implements RollingView, KeyDispatchable {
	private static final String tag = FragmentRolling.class.getSimpleName();

    public static FragmentRolling newInstance() {
        FragmentRolling obj = new FragmentRolling();
        return obj;
    }

    @BindView(R.id.rolling_container)
    HorizontalRollingContainer mContainer;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        Log.d(tag, "onCreateView()");
		View view = inflater.inflate(R.layout.fragment_rolling, container, false);
        ButterKnife.bind(this, view);
		return view;
	}

    @Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
        Log.d(tag, "onActivityCreated() activity : ");
        initialize();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public RollingPresenter createPresenter() {
        return new RollingPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
	public boolean dispatchKeyEvent(KeyEvent event) {
        final int action = event.getAction();
        final int keyCode = event.getKeyCode();
        Log.d(tag, String.format("dispatchKeyEvent()[%s][%s]",
                ""+action, ""+keyCode));
        if (keyCode == KeyEvent.KEYCODE_POWER) {
            return false;
        }

        if (action == KeyEvent.ACTION_UP) {
            if(keyCode == KeyEvent.KEYCODE_BACK) {
                FragmentLoader.removeFragment(getActivity().getSupportFragmentManager(), this);
                getActivity().finish();
                return true;
            }
		}
		return false;
	}

    private void initialize() {
        presenter.initialize(mContainer);
    }


    @OnClick({R.id.rolling_index01, R.id.rolling_index02})
    public void setEvent(View v) {
        if(v.getId()==R.id.rolling_index01) {
            presenter.onEvent(true);
        } else {
            presenter.onEvent(false);
        }
    };

    @OnClick(R.id.rolling_index03)
    public void setEventRefresh(View v) {
        presenter.onEventRefresh();
    };

}
