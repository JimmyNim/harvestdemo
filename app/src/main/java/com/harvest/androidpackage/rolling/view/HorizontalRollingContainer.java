package com.harvest.androidpackage.rolling.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.Scroller;

import com.harvest.androidpackage.R;
import com.harvest.tvs.util.Log;

import java.util.ArrayList;
import java.util.List;


public class HorizontalRollingContainer extends ViewGroup {
    private static final String tag = HorizontalRollingContainer.class.getSimpleName();

    private final Scroller mScroller;
    private final GestureDetector mGestureDetector;

    private int[] resIndex = {
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9
    };

    class EventItem {
        public final String id;
        public final String text;
        public EventItem(String id, String text) {
            this.id = id;
            this.text = text;
        }
    }
    List<EventItem> mItemms = new ArrayList<>();

    public HorizontalRollingContainer(Context context) {
        this(context, null);
    }

    public HorizontalRollingContainer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    private final Rect mDrawingRect;
    private final Rect mMeasuringRect;
    private final Rect mClipRect;
    private final Paint mPaint;
    private final int mVodWidth;
    private final int mVodHeight;
    private final int mVodMarginWidth;
    private final int mVodMarginTop;
    private final int mVodItemRectColor;
    private final int mVodTextColor;
    private final int mVodTextSize;

    public HorizontalRollingContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setWillNotDraw(false);
        for(Integer in : resIndex) {
            EventItem item = new EventItem(""+in, "Text="+in);
            mItemms.add(item);
        }
        mDrawingRect = new Rect();
        mClipRect = new Rect();
        mMeasuringRect = new Rect();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mVodWidth = 100;
        mVodHeight = 150;
        mVodMarginTop = 100;
        mVodMarginWidth = 100;
        mVodTextSize = 30;
        mVodItemRectColor = ContextCompat.getColor(context, R.color.AliceBlue);
        mVodTextColor = ContextCompat.getColor(context, R.color.Chocolate);
        mGestureDetector = new GestureDetector(context, new OnGestureListener());
        mScroller = new Scroller(context);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        Rect drawingRect = mDrawingRect;
        drawingRect.left = getScrollX();
        drawingRect.top = getScrollY();
        drawingRect.right = drawingRect.left + getWidth();
        drawingRect.bottom = drawingRect.top + getHeight();
        drawViewLayer(canvas, drawingRect);

        // If scroller is scrolling/animating do scroll. This applies when doing a fling.
        if (mScroller.computeScrollOffset()) {
            scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
        }
    }

    private void drawViewLayer(Canvas canvas, Rect drawingRect) {
        final int first = getFirstVisiblePosition();
        Log.d(tag, String.format("drawViewLayer()[%s][%s][%s]-[%s]", "===============",
                ""+getScrollX() +"/"+getScrollY(),
                ""+getWidth() +"/"+getHeight(),
                ""+first
        ));

        int itemSize = mItemms.size();
        int index = 0;
        for(int pos=first; pos<first+5; pos++) {
            int temp = pos % itemSize;
            if (temp < 0) {
                index = itemSize + temp;
            } else {
                index = temp % itemSize;
            }
            Log.d(tag, String.format("drawViewLayer()[%s][%s]",
                    ""+pos, ""+index));
            EventItem item = mItemms.get(index);
            drawVodItem(canvas, pos, item, drawingRect);
        }
    }

    private void drawVodItem(final Canvas canvas, final int position, final EventItem item, final Rect drawingRect) {
        drawingRect.left = getXFrom(position);
        drawingRect.top = mVodMarginTop;
        drawingRect.right = getXFrom(position) + mVodWidth;
        drawingRect.bottom = drawingRect.top + mVodHeight;
        mPaint.setColor(mVodItemRectColor);
        canvas.drawRect(drawingRect, mPaint);
        // text
        // Move drawing.top so text will be centered (text is drawn bottom>up)
        String title = item.text;
        mPaint.getTextBounds(title, 0, title.length(), mMeasuringRect);
        drawingRect.top += ((drawingRect.bottom - drawingRect.top) / 2) + (mMeasuringRect.height() / 2);
        mPaint.setColor(mVodTextColor);
        mPaint.setTextSize(mVodTextSize);
        title = title.substring(0, mPaint.breakText(title, true, drawingRect.right - drawingRect.left, null));
        canvas.drawText(title, drawingRect.left, drawingRect.top, mPaint);
        canvas.save();
    }

    private int getXFrom(int position) {
        return (int) position * (mVodWidth + mVodMarginWidth);
    }

    /**
     *  Get the position of Scroll X.
     *  x value is divided by Item' width and margin' width.
     *  You must calibrate x position with "revisedX" value.
     *  When you move 100px to x axis, you only get x value less than 100px onDraw(Canvas canvas).
     *  Therefore, need to revise x axis.
     *
     * @return position X position by getScrollX()
     */
    private int getFirstVisiblePosition() {
        final int x = getScrollX();
        final int revisedX;
        if(x < 0) {
            revisedX = x - mVodMarginWidth/2;
        } else {
            revisedX = x + mVodMarginWidth/2;
        }
        int position = revisedX / (mVodWidth + mVodMarginWidth);
        Log.d(tag, String.format("getFirstVisiblePosition()[%s][%s]", ""+revisedX, ""+position));
//        if (position < 0) {
//            position = 0;
//        }
        return position;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        recalculateAndRedraw(false);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    }

    public void recalculateAndRedraw(boolean withAnimation) {
        int scrollX = 0;
        int scrollY = 0;
        int dx = 0;
        int dy = 0;
        mScroller.startScroll(scrollX, scrollY,
                dx, dy, withAnimation ? 600 : 0);
        redraw();
    }

    public void recalculateAndRedraw() {
        final int pos = getFirstVisiblePosition();
        if(pos > 10) {
            Log.d(tag, String.format("recalculateAndRedraw()[%s]-[%s][%s]",
                    "", "RollBack", ""));
            int tempPos = pos % mItemms.size();
            int x = getXFrom(tempPos);
            mScroller.startScroll(x, getScrollY(),
                    0, 0, 0);
            redraw();
            return;
        }
    }

    /**
     * Does a invalidate() and requestLayout() which causes a redraw of screen.
     */
    public void redraw() {
        invalidate();
        requestLayout();
    }

    public void onClickEvent(boolean isLeftClicked) {
        final int pos = getFirstVisiblePosition();
        boolean withAnimation = true;
        int scrollX = getXFrom(pos);
        int scrollY = getScrollY();
        Log.d(tag, String.format("onClickEvent()[%s]-[%s][%s]",
                ""+isLeftClicked,""+scrollX, ""+scrollY));
        int dx = 0;
        if(isLeftClicked) {
            dx -= (mVodWidth + mVodMarginWidth);
        } else {
            dx += (mVodWidth + mVodMarginWidth);
        }
        int dy = 0;
        mScroller.startScroll(scrollX, scrollY,
                dx, dy, withAnimation ? 600 : 0);
        redraw();
    }

    private class OnGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            Log.v(tag, "onSingleTapUp(...)");

            if (mItemms.isEmpty()) {
                return false;
            }
            // This is absolute coordinate on screen not taking scroll into account.
            final int x = (int) e.getX();
            final int y = (int) e.getY();
            Log.d(tag, "absolute coordinate (" + x + "," + y + ")");
            // Adding scroll to clicked coordinate
            final int scrollX = getScrollX() + x;
            final int scrollY = getScrollY() + y;
            Log.d(tag, "relative coordinate (" + scrollX + "," + scrollY + ")");
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            int x = getScrollX();
            int y = getScrollY();
            int dx = (int) distanceX;
            int dy = (int) distanceY;
            Log.d(tag, String.format("onScroll()[%s][%s]-[%s][%s]",
                    ""+x,""+y, ""+distanceX, ""+distanceY));
            scrollBy(dx, dy);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float vX, float vY) {
            redraw();
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            if (!mScroller.isFinished()) {
                mScroller.forceFinished(true);
                return true;
            }
            return true;
        }
    }
}
