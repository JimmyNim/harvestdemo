package com.harvest.androidpackage.rolling;

import android.view.View;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.harvest.androidpackage.R;
import com.harvest.androidpackage.rolling.view.HorizontalRollingContainer;

/**
 * Created by jimmy.lee on 2016. 2. 29..
 */
public class RollingPresenter extends MvpBasePresenter<RollingView> {
    private static final String tag = RollingPresenter.class.getSimpleName();

    HorizontalRollingContainer mVod;
    public void initialize(View view) {
        mVod = (HorizontalRollingContainer) view.findViewById(R.id.rolling_container);
    };

    public void onEvent(boolean isLeftClicked) {
        mVod.onClickEvent(isLeftClicked);
    }

    public void onEventRefresh() {
        mVod.recalculateAndRedraw();
//        mVod.recalculateAndRedraw(true);
    }
}
