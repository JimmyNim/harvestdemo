package com.harvest.androidpackage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SimpleAdapter;

import com.harvest.androidpackage.common.FragmentLoader;
import com.harvest.tvs.util.Log;

public class MainActivity extends AppCompatActivity {
    private static final String tag = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize() {
        FragmentLoader.loadFragment(getSupportFragmentManager(), FragmentMain.newInstance(), R.id.fragment);
    }

    @Override
    public void onBackPressed() {
        Log.d(tag, String.format("onBackPressed()[%s][%s]",
                ""+getSupportFragmentManager().getBackStackEntryCount(),""));
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        }
        super.onBackPressed();
    }

}
