package com.harvest.androidpackage;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;


import com.harvest.androidpackage.common.PreferenceManager;
import com.harvest.tvs.util.Log;

import java.lang.reflect.Field;
import java.util.Stack;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;


import static java.util.concurrent.Executors.newCachedThreadPool;

/**
 * Created by JimmyLee on 2016-01-06.
 */
public class MainApplication extends Application {

    private final static String tag = MainApplication.class.getSimpleName();

    private static final Executor backgroundExecutor =
            newCachedThreadPool(new ThreadFactory() {
                @Override
                public Thread newThread(Runnable r) {
                    return new Thread(r);
                }
            });
    public static MainApplication INSTANCE;

    Stack<Activity> mActivityStack = new Stack<>();

    public static String getVersionInfo(Context context) {
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            String versionName = pi.versionName;
            String versionCode = String.valueOf(pi.versionCode);
            Log.d(tag, String.format("debugVersionCode()VersionName[%s]VersionCode[%s]", versionName, versionCode));
            return "" + versionName + "/" + versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void fixLGBubblePopupHelper(final Application application) {
        backgroundExecutor.execute(new Runnable() {
            @Override
            public void run() {
                final Field sHelperField;
                try {
                    Class<?> bubbleClass = Class.forName("android.widget.BubblePopupHelper");
                    sHelperField = bubbleClass.getDeclaredField("sHelper");
                    sHelperField.setAccessible(true);
                } catch (Exception ignored) {
                    // We have no guarantee that this class / field exists.
                    return;
                }
                application.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
                    @Override
                    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

                    }

                    @Override
                    public void onActivityStarted(Activity activity) {

                    }

                    @Override
                    public void onActivityResumed(Activity activity) {

                    }

                    @Override
                    public void onActivityPaused(Activity activity) {

                    }

                    @Override
                    public void onActivityStopped(Activity activity) {

                    }

                    @Override
                    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

                    }

                    @Override
                    public void onActivityDestroyed(Activity activity) {
                        try {
                            sHelperField.set(null, null);
                        } catch (IllegalAccessException ignored) {
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onCreate() {
        Log.v(tag, "onCreate()");
        super.onCreate();
        INSTANCE = this;
        init();
    }

    @Override
    public void onTerminate() {
        Log.d(tag, "onTerminate()");
        super.onTerminate();
    }

    public void init() {
        mActivityStack.clear();
        Log.d(tag, "cache directory : " + getBaseContext().getCacheDir().getAbsoluteFile());
        PreferenceManager.INSTANCE.init(getApplicationContext());
    }

    // close application by top button & broadcast from MainMenu
    public void finishAllActivities(Context context) {
        try {
            while (!mActivityStack.empty()) {
                Activity activity = mActivityStack.pop();
                Log.d(tag, "finish " + activity.getClass().toString());
                activity.finish();
            }
        } catch (Exception e) {
            Log.d(tag, "" + e.toString());
        }
        mActivityStack = null;
    }


    /**
     * @return ex)
     * if(mApplication.getActivityStack() != null)
     * mApplication.getActivityStack().push(this);
     */
    public Stack<Activity> getActivityStack() {
        return mActivityStack;
    }

    public String getAppVersion(Context ctx) {
        String version;

        try {
            PackageInfo i = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            version = i.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version = null;
        }

        return version;
    }

    private void loadNMPSDKLibrary() {
        System.loadLibrary("nmpsdk");
    }
}
