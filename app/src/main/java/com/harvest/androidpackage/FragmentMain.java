package com.harvest.androidpackage;

import android.app.FragmentManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.harvest.androidpackage.common.FragmentLoader;
import com.harvest.androidpackage.common.KeyDispatchable;
import com.harvest.androidpackage.rolling.FragmentRolling;
import com.harvest.androidpackage.weather.OverlayDialog;
import com.harvest.tvs.util.Log;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentMain extends MvpFragment<MainView, MainPresenter> implements MainView, KeyDispatchable {
    private static final String tag = FragmentMain.class.getSimpleName();

    public static FragmentMain newInstance() {
        FragmentMain obj = new FragmentMain();
        return obj;
    }

    @BindView(R.id.main_index01)
    ListView mListView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(tag, "onCreateView()");
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(tag, "onActivityCreated() activity : ");
        initialize();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        final int action = event.getAction();
        final int keyCode = event.getKeyCode();
        Log.d(tag, String.format("dispatchKeyEvent()[%s][%s]",
                "" + action, "" + keyCode));
        if (keyCode == KeyEvent.KEYCODE_POWER) {
            return false;
        }

        if (action == KeyEvent.ACTION_UP) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                FragmentLoader.removeFragment(getActivity().getSupportFragmentManager(), this);
                getActivity().finish();
                return true;
            }
        }
        return false;
    }


    public String[] mArrayFragment = {
            FragmentRolling.class.getSimpleName(),
            OverlayDialog.class.getSimpleName(),
            "None"
    };

    private void initialize() {
        presenter.initialize();
        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.list_item, mArrayFragment);
        if(mListView!=null) {
            mListView.setAdapter(adapter);
            mListView.deferNotifyDataSetChanged();
            mListView.setOnItemClickListener((parent, view, position, id) -> {
                Log.d(tag, String.format("onItemClick()[%s]", ""+position, ""+id));
                onEventPosition(position);
            });
        } else {
            Log.d(tag, String.format("onItemClick()[%s]", "ListView Null!!!", ""));
        }
    };

    private void onEventPosition(int position) {
        switch (position) {
            case 0:
                FragmentLoader.loadFragment(getFragmentManager(),
                        FragmentRolling.newInstance(), R.id.fragment);
                break;
            case 1:
                FragmentManager fm = getActivity().getFragmentManager();
                OverlayDialog dialog = new OverlayDialog();
                dialog.show(fm, OverlayDialog.class.getSimpleName());
                break;

        }


    }
}

