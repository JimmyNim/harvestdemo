package com.harvest.androidpackage.common;

import android.view.KeyEvent;

public interface KeyDispatchable {
	public boolean dispatchKeyEvent(KeyEvent event);
}
