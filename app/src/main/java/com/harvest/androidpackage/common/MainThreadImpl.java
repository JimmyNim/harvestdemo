package com.harvest.androidpackage.common;

import android.os.Handler;
import android.os.Looper;

import com.harvest.tvs.util.Log;


public class MainThreadImpl implements IMainThread {

	private static final String tag = MainThreadImpl.class.getSimpleName();

    private Handler handler;
    public MainThreadImpl() {
        this.handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void post(Runnable runnable) {
        handler.post(runnable);
    }

    // for test
    public static void main(String[] args) {
        Log.setLogger(new Log.JavaLog());
        Log.d(tag, "MAIN START");
        final Object o = new Object();
        String str = "com.age.debug";
        int offset = str.lastIndexOf(".");
        System.out.println(String.format("main()[%s][%s]", ""+str.substring(0, offset), ""+str.lastIndexOf(".")));
        long time = 11100;
        System.out.println(String.format("main()[%s][%s]", ""+time%1000, ""));

        synchronized (o) {
            try {
                o.wait(100000);
                Log.d(tag, "MAIN END");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
