package com.harvest.androidpackage.common;



public interface IMainThread {

    public abstract void post(final Runnable runnable);

}
