package com.harvest.androidpackage.common;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.harvest.tvs.util.Log;
import com.harvest.tvs.yahoo.WeatherItem;

import java.util.Calendar;
import java.util.Locale;

public enum PreferenceManager {
    INSTANCE;
    private static final String tag = PreferenceManager.class.getSimpleName();

    private static final String PREF_NAME = "Harvest";

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private Gson mGson;

    public void init(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        mGson = new GsonBuilder().create();
    }

    private String getString(String key, String defValue) {
        if (mSharedPreferences == null) {
            throw new NullPointerException("SharedPreferences must not be null.");
        }
        return mSharedPreferences.getString(key, defValue);
    }

    /*
     * Weather Info.
     */
    private static final String WEATHERE_KEY_SAVED_TIME = "weather.time";

    public long getSavedTimeMillisOfWeather() {
        long result = mSharedPreferences.getLong(WEATHERE_KEY_SAVED_TIME, 0);
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(result);
        Log.d(tag, String.format("getSavedTimeMillisOfWeather()[%s][%s]", "" + c.getTime(), ""));
        return result;
    }

    public void setSavedTimeMillisOfWeather() {
        Calendar c = Calendar.getInstance();
        mEditor.putLong(WEATHERE_KEY_SAVED_TIME, c.getTimeInMillis());
        mEditor.commit();
        Log.d(tag, String.format("setSavedTimeMillisOfWeather()[%s][%s]", "" + c.getTime(), ""));
    }

    public final static class NetworkInfo {
        public final String ipAddress;
        public final int port;
        public NetworkInfo(String ipAddress, int port) {
            this.ipAddress = ipAddress;
            this.port = port;
        }
    }

    final String KEY_NETWORKINFO = "NetworkInfo";
    public boolean setNetworkInfo(NetworkInfo item) {
        Log.d(tag, String.format("setNetworkInfo()[%s][%s]", ""+item.ipAddress, ""+item.port));
        NetworkInfo info = new NetworkInfo(item.ipAddress, item.port);
        mEditor.putString(KEY_NETWORKINFO, mGson.toJson(info));
        mEditor.commit();
        return true;
    }

    public NetworkInfo getNetworkInfo() {
        NetworkInfo info = null;
        String json = mSharedPreferences.getString(KEY_NETWORKINFO, null);
        if (json != null) {
            info = mGson.fromJson(json, NetworkInfo.class);
            Log.d(tag, String.format("getNetworkInfo()[%s][%s]", ""+info.ipAddress, ""+info.port));
        } else {
            Log.d(tag, String.format("getNetworkInfo()[%s][%s]", "NULL !!!!", ""));
        }
        return info;
    }


    public final static class Weather {
        public final int woeid;
        public final String city;
        public final String temperature_code;
        public final String temperature;

        public Weather(int woeid, String city, String temperature_code, String temperature) {
            this.woeid = woeid;
            this.city = city;
            this.temperature_code = temperature_code;
            this.temperature = temperature;
        }
    }

    public boolean setWeatherInfo(WeatherItem item) {
        setSavedTimeMillisOfWeather();
        Weather weather = new Weather(item.woeid, item.getLocationCity(), item.getTemperatureCode(), item.getTemperature());
        mEditor.putString(String.valueOf(item.woeid), mGson.toJson(weather));
        mEditor.commit();
        return true;
    }

    public Weather getWeatherInfo(int woeid) {
        Weather weather = null;
        String json = mSharedPreferences.getString(String.valueOf(woeid), null);
        if (json != null) {
            weather = mGson.fromJson(json, Weather.class);
        } else {
            Log.d(tag, String.format("getWeatherInfo()[%s][%s]", "NULL !!!!", "" + woeid));
        }
        return weather;
    }

    private final String LOCALE = "Locals";
    private final String LOCALE_DEFAULT = "en_US";
    public Locale getLocale(Context context) {
        String locale = mSharedPreferences.getString(LOCALE, LOCALE_DEFAULT);
        Configuration config = context.getResources().getConfiguration();
        config.locale = new Locale(locale);
        Log.d(tag, String.format("getLocale()[%s][%s]",
                ""+config.locale.toString(), ""+config.locale.getDisplayCountry().toLowerCase()));
        return config.locale;
    }
    public String updateLocale(String locale) {
        String result = "en_US";
        mEditor.putString(LOCALE, locale);
        mEditor.commit();
        result = mSharedPreferences.getString(LOCALE, LOCALE_DEFAULT);
        Log.d(tag, String.format("updateLocale()result[%s]input[%s]", ""+result, ""+locale));
        return result;
    }

    private final String FIRST_CONNECTION = "first";
    public boolean isFirstConnnection() {
        boolean result = mSharedPreferences.getBoolean(FIRST_CONNECTION, true);
        if(result) {
            mEditor.putBoolean(FIRST_CONNECTION, false);
            mEditor.commit();
        }
        Log.d(tag, String.format("isFirstConnnection()[%s][%s]", ""+result, ""));
        return result;
    }

    public final static class ScanInfo {
        public final int startFreq;
        public final int endFreq;
        public final int bandwidth;
        public final int symbolRate;

        public ScanInfo(int startFreq, int endFreq, int bandwidth, int symbolRate) {
            this.startFreq = startFreq;
            this.endFreq = endFreq;
            this.bandwidth = bandwidth;
            this.symbolRate = symbolRate;
        }

        @Override
        public String toString() {
            return ""+startFreq+","+endFreq+","+bandwidth+","+symbolRate;
        }
    }

    private final String SCAN_INFO = "scan_info";
    public boolean setScanInfo(ScanInfo item) {
        Log.d(tag, String.format("setScanInfo[%s]", ""+item.toString()));
        ScanInfo weather = new ScanInfo(item.startFreq, item.endFreq, item.bandwidth, item.symbolRate);
        mEditor.putString(SCAN_INFO, mGson.toJson(weather));
        mEditor.commit();
        return true;
    }

    public ScanInfo getScanInfo() {
        ScanInfo result = null;
        String json = mSharedPreferences.getString(SCAN_INFO, null);
        if (json != null) {
            result = mGson.fromJson(json, ScanInfo.class);
            Log.d(tag, String.format("getScanInfo()[%s][%s]", "" + result, ""));
        } else {
            Log.d(tag, String.format("getScanInfo()[%s][%s]", "NULL !!!!", "" + SCAN_INFO));
        }
        return result;
    }
}
