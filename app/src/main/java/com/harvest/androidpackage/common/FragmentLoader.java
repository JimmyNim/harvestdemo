package com.harvest.androidpackage.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.harvest.androidpackage.R;

public class FragmentLoader {

	private static final String tag = FragmentLoader.class.getSimpleName();

	public static void loadFragment(FragmentManager manager, Fragment fragment, int containerId) {
		loadFragment(manager, fragment, containerId, false);
	}
	
	public static void loadFragment(FragmentManager manager, Fragment fragment, int containerId, boolean isMultiDepth) {
		if (fragment == null)
			return;
		
        final FragmentTransaction mFragmentTransaction =
                manager.beginTransaction();

        final Fragment prevFragment;
//        if (isMultiDepth) {
//            prevFragment = getPrevFragment(manager, fragment);
//        } else {
//            prevFragment = null;
//        }

        if (isMultiDepth) {
//            mFragmentTransaction.hide(prevFragment);
            // TODO : is this right?
//            mFragmentTransaction.setCustomAnimations(
//                    R.anim.xml_anim_slide_in_right, R.anim.xml_anim_slide_out_right,
//                    R.anim.xml_anim_slide_in_right, R.anim.xml_anim_slide_out_right);
            mFragmentTransaction.add(R.id.fragment, fragment, fragment.getClass().getName());
            mFragmentTransaction.addToBackStack(fragment.getClass().getName());
            /** This is the part that will cause a fragment to be added to back stack */
            mFragmentTransaction.commitAllowingStateLoss();
        } else {
            /** Put the fragment in place */
            mFragmentTransaction.replace(containerId, fragment, fragment.getClass().getName());
            mFragmentTransaction.addToBackStack(fragment.getClass().getName());
            mFragmentTransaction.commit();
        }

    }

    public static void addFragment(FragmentManager manager, Fragment currentFragment, Fragment targetFragment, int containerId) {
        if (manager == null)
            return;
        if (currentFragment == null)
            return;
        if (targetFragment == null)
            return;

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.hide(currentFragment);
        // use a fragment tag, so that later on we can find the currently displayed fragment
        transaction.add(containerId, targetFragment, targetFragment.getClass().getName())
                .commit();
//        transaction.add(containerId, targetFragment, targetFragment.getClass().getName())
//                .addToBackStack(targetFragment.getClass().getName())
//                .commit();
    }

    public static void removeFragment(FragmentManager manager, Fragment fragment) {
        if (manager == null)
            return;
        if (fragment == null)
            return;

        //                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.remove(fragment);
        Fragment prevFragment = getPrevFragment(manager);
        if (prevFragment!=null) {
            transaction.show(prevFragment);
        }
        transaction.commit();
    }

	private static Fragment getPrevFragment(FragmentManager manager) {
        String prevFragTag = null;
        Fragment prevFragment = null;
        if(manager.getBackStackEntryCount()!=0) {
            prevFragTag = manager.getBackStackEntryAt(manager.getBackStackEntryCount()-1).getName();
            if (prevFragTag != null) {
                prevFragment = manager.findFragmentByTag(prevFragTag);
            } else {
                prevFragment = null;
            }
        } else {
            prevFragment = null;
        }
        return prevFragment;
	}

}
