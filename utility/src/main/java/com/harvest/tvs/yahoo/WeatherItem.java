package com.harvest.tvs.yahoo;

import java.io.Serializable;

/**
 * Created by Jimmy on 2016-03-11.
 */
public class WeatherItem implements Serializable {
    public int woeid = -1;
    private Query query;

    public String getLocationCity() {
        return query.results.channel.location.city;
    }

    public String getLocationCountry() {
        return query.results.channel.location.country;
    }

    public String getLocationRegion() {
        return query.results.channel.location.region;
    }

    public String getTemperature() {
        return query.results.channel.item.condition.temp;
    }

    public String getTemperatureCode() {
        return query.results.channel.item.condition.code;
    }

    public String getConditionDate() {
        return query.results.channel.item.condition.date;
    }

    protected class Query {
        private int count;
        private String created;
        private String lang;
        private Result results;
    }

    public static class Result {
        private Channel channel;
    }

    public static class Channel {
        private Location location;
        private Item item;
    }

    public class Location {
        private String city;
        private String country;
        private String region;
    }

    public static class Item {
        private Condition condition;
    }

    public class Condition {
        private String code;
        private String date;
        private String temp;
        private String text;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(WeatherItem.class.getSimpleName()+":");
        sb.append(woeid);
        sb.append(", ");
        sb.append(query.count);
        sb.append(", ");
        sb.append(query.created);
        sb.append(", ");
        sb.append(query.results.channel.item.condition.date);
        sb.append(", ");
        sb.append(query.results.channel.item.condition.temp);
        sb.append(", ");
        sb.append(query.results.channel.item.condition.text);
        sb.append(", ");
        return sb.toString();
    }
}
