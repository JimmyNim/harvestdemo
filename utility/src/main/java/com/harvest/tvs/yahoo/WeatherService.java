package com.harvest.tvs.yahoo;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 *
 */
public interface WeatherService {

    public static final String URL_FORECAST = "https://query.yahooapis.com/";
    public static final String PREFIX_QUERY = "select location, item.condition from weather.forecast where woeid = %d and u='c'";
    public static final String FORMAT_JSON = "json";
    public static final String ENV = "store://datatables.org/alltableswithkeys";
    public static final Integer[] KEY_CITY_IDS = {
        1047378, 1044316,1047180
    };
    /**
     *  CITY = {"Jakarta","Surabaya","Bandung"};
     *  CITY_ID = {"1047378","1044316","1047180"};
     *  URL Info
     *  u='c' : celsius
     *  https://query.yahooapis.com/v1/public/yql?q=select location, item.condition from weather.forecast where woeid = 2487889 and u='c'&format=json&env=store://datatables.org/alltableswithkeys
     *  https://query.yahooapis.com/v1/public/yql?q=select location, item.condition from weather.forecast where woeid = 1044316 and u='c'&format=json&env=store://datatables.org/alltableswithkeys
     */
    @GET("v1/public/yql")
    Observable<WeatherItem> forecastrss(
            @Query(value = "q") String areaId,
            @Query(value = "format") String format,
            @Query(value = "env") String tail
    );

    /*
    <integer-array name="YahooWoeids">
    <item>44418</item>     		            <!--  London, GMT+0 -->
    <item>638242</item>					    <!-- Berlin, GMT+1 -->
    <item>946738</item>					    <!-- Athens, GMT+2 -->
    <item>2122265</item>					<!-- Moscow, GMT+3 -->
    <item>1940330</item>					<!-- Abu Dhabi, GMT+4 -->

    <item>2211027</item>					<!-- Islamabad, GMT+5 -->
    <item>2255777</item>					<!-- Almaty, GMT+6 -->
    <item>1047378</item>					<!-- Jakarta, GMT+7 -->
    <item>2151330</item>					<!-- Beijing, GMT+8 -->
    <item>1132599</item>					<!-- Seoul, GMT+9 -->

    <item>1105779</item>				    <!-- Sydney, GMT+10 -->
    <item>1049640</item>     	            <!--  Noumea , GMT+11 -->
    <item>2351310</item>				    <!-- Wellington, GMT+12 -->
    <item>1062841</item>				    <!-- Pago Pago, GMT-11 -->
    <item>2347570</item>					<!-- Hawaii, GMT-10 -->

    <item>2347560</item>					<!-- Alaska, GMT-9 -->
    <item>2442047</item>			        <!-- Los Angeles, GMT-8 -->
    <item>2391279</item>					<!-- Denver, GMT-7 -->
    <item>2379574</item>				    <!-- Chicago, GMT-6 -->
    <item>2459115</item>				    <!-- New York, GMT-5 -->

    <item>346057</item>					    <!-- La Paz, GMT-4 -->
    <item>455827</item>				        <!-- Sao Paulo, GMT-3 -->
    <item>12580782</item>				    <!-- Fernando de Noronha, GMT-2 -->
    <item>15021776</item>				    <!-- Azores, GMT-1 -->
    </integer-array>
    */
    /**
     *
     * Yahoo Weather URL
     * http://query.yahooapis.com/v1/public/yql?q=select location, item.condition from weather.forecast where woeid=1132599 and u='c'&format=json&env=store://datatables.org/alltableswithkeys
     */
    public static final Integer[] WOEIDs = {
            44418, 638242, 946738, 2122265, 1940330,
            2211027, 2255777, 1047378, 2151330, 1132599,
            1105779, 1049640, 2351310, 1062841, 2347570,
            2347560, 2442047, 2391279, 2379574, 2459115,
            346057, 455827, 12580782, 15021776
    };

}
