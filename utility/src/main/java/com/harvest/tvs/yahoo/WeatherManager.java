package com.harvest.tvs.yahoo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.harvest.tvs.util.Log;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.schedulers.Schedulers;


/**
 * Created by Jimmy on 2016-03-11.
 */
public class WeatherManager {
    private static final String tag = WeatherManager.class.getSimpleName();
    public static final WeatherManager INSTANCE = new WeatherManager();

    private final RxJavaCallAdapterFactory mRxJavaCallAdapterFactory = RxJavaCallAdapterFactory.create();
    private final GsonConverterFactory mGsonConverterFactory = GsonConverterFactory.create(
            new GsonBuilder().registerTypeAdapterFactory(new ImmutableListTypeAdapterFactory()).create());

    private final WeatherService mWeatherService;
//    @Inject
//    OkHttpClient mOkHttpClient;
    private WeatherManager() {
        mWeatherService = buildService(WeatherService.URL_FORECAST, mGsonConverterFactory, WeatherService.class);
    }
    private <T> T buildService(String url, Converter.Factory converter, Class<T> service) {//, ResponseType responseType) {
        return new Retrofit.Builder()
                //.client(mOkHttpClient)
                .baseUrl(url)
                .addCallAdapterFactory(mRxJavaCallAdapterFactory)
                .addConverterFactory(converter)
                .build()
                .create(service);
    }

    public Observable<WeatherItem> getWeatherService(Integer[] woeids) {
        Log.d(tag, String.format("getWeatherService()[%s][%s]", "", "" + woeids));
        return  Observable.from(woeids)
                .flatMap(s -> {
                    return mWeatherService.forecastrss(String.format(WeatherService.PREFIX_QUERY, s), WeatherService.FORMAT_JSON, WeatherService.ENV)
                            .map(weatherItem -> {
                                Log.d(tag, String.format("getWeatherService(1)[%s][%s]", "", ""));
                                weatherItem.woeid = s;
                                return weatherItem;
                            })
                            .retryWhen(new RxHelper.RetryWithDelay(1, 2000));
                });
    }

    public Observable<WeatherItem> getWeatherServiceByGMT() {
        String gmtOffset = getSystemGMTOffSet();
        return getWeatherServiceByWoeid(getForcastWoeId(gmtOffset));
    }

    public Observable<WeatherItem> getWeatherServiceByGMT(String gmtOffSet) {
        return getWeatherServiceByWoeid(getForcastWoeId(gmtOffSet));
    }

    public Observable<WeatherItem> getWeatherServiceByWoeid(int woeid) {
        Log.d(tag, String.format("getWeatherServiceByWoeid()[%s][%s]", ""+woeid, ""));
        return mWeatherService.forecastrss(String.format(WeatherService.PREFIX_QUERY, woeid), WeatherService.FORMAT_JSON, WeatherService.ENV)
                .map(weatherItem -> {
                    Log.d(tag, String.format("getWeatherServiceByWoeid(1)[%s][%s]", "" + woeid, ""));
                    weatherItem.woeid = woeid;
                    return weatherItem;
                })
                .retryWhen(new RxHelper.RetryWithDelay(1, 2000));
    }

    private static class ImmutableListTypeAdapterFactory implements TypeAdapterFactory {

        @Override
        public <T> TypeAdapter<T> create(Gson gson, final TypeToken<T> type) {
            final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);

            return new TypeAdapter<T>() {
                @Override
                public void write(JsonWriter out, T value) throws IOException {
                    delegate.write(out, value);
                }

                @Override
                public T read(JsonReader in) throws IOException {
                    T t = delegate.read(in);
                    if (List.class.isAssignableFrom(type.getRawType())) {
                        return (T) Collections.unmodifiableList((List<?>) t);
                    }
                    return t;
                }
            };
        }
    }

    private String getSystemGMTOffSet() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("Z");
        String localTime = date.format(currentLocalTime);
        Log.d(tag, String.format("getGMTOffSet()[%s][%s]", "", ""+localTime));
        return localTime;
    }

    public Integer getForcastWoeIdInSystemTimeZone() {
        String gmtOffset = getSystemGMTOffSet();
        return getForcastWoeId(gmtOffset);
    }

    public Integer getForcastWoeId(String gmtOffset) {
//        gmtOffset = "-1300";
        int woeId = WeatherService.WOEIDs[0];
        int tempGMT = -100;
        try {
            char plus = gmtOffset.charAt(0);
            if('+'==plus) {
                tempGMT = Integer.valueOf(gmtOffset.substring(1, 3));
            } else {
                System.out.println("getForcastWoeId(2)");
                tempGMT = -Integer.valueOf(gmtOffset.substring(1, 3));
            }
        } catch (StringIndexOutOfBoundsException e) {
            tempGMT = -100;
            System.out.println(String.format("getForcastWoeId().StringIndexOutOfBoundsException![%s]",
                    ""+tempGMT));
        }
        if(tempGMT >= -11 && tempGMT <=12) {
            if(tempGMT >= 0) {
                woeId = WeatherService.WOEIDs[tempGMT];
            } else {
                System.out.println(String.format("getForcastWoeId()[%s][%s]",
                        ""+WeatherService.WOEIDs.length, ""+ tempGMT));
                woeId = WeatherService.WOEIDs[(WeatherService.WOEIDs.length + tempGMT)];
            }
        } else {
            System.out.println(String.format("getForcastWoeId().GMT INFO ERROR !!![%s]", ""+tempGMT));
        }
        return woeId;
    }

    // for test
    public static void main(String[] args) {
        Log.setLogger(new Log.JavaLog());
        Log.d(tag, "MAIN START");
        final Object o = new Object();

//        INSTANCE.testWeatherService();
//        INSTANCE.testWeatherServiceByGMT();
        synchronized (o) {
            try {
                o.wait(100000);
                Log.d(tag, "MAIN END");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void testWeatherService() {
        //        String[] AREA = {"Jakarta","Surabaya","Bandung"};
//        String[] AREA_ID = {"1047378","1044316","1047180"};
//        WeatherItems.put(1047378, "Jakarta");
//        WeatherItems.put(1044316, "Surabaya");
//        WeatherItems.put(1047180, "Bandung");
//        for(int i=0; i<WeatherService.KEY_CITY_IDS.length; i++) {
//            INSTANCE.WeatherItems.put(WeatherService.KEY_CITY_IDS[i], WeatherService.VALUE_CITYS[i]);
//        }
//        INSTANCE.getWeatherService(INSTANCE.WeatherItems.keySet().toArray(new Integer[0]))
        INSTANCE.getWeatherService(WeatherService.KEY_CITY_IDS)
                .subscribeOn(Schedulers.io())
                .subscribe(weatherItem -> {
                    Log.d(tag, String.format("subscribe()[%s][%s]", ""+weatherItem.toString(), ""+Thread.currentThread()));
                }, throwable -> {
                    throwable.printStackTrace();
                },  ()-> {
                    Log.d(tag, String.format("complieted()[%s][%s]", "", ""+Thread.currentThread()));
                });
    }

    private void testWeatherServiceByGMT() {
        INSTANCE.getWeatherServiceByGMT()
                .subscribeOn(Schedulers.io())
                .subscribe(weatherItem -> {
                    Log.d(tag, String.format("subscribe()[%s][%s]", ""+weatherItem.toString(), ""+Thread.currentThread()));
                }, throwable -> {
                    throwable.printStackTrace();
                },  ()-> {
                    Log.d(tag, String.format("complieted()[%s][%s]", "", ""+Thread.currentThread()));
                });
    }

}
