package com.harvest.tvs.util;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarManager {

    public static Calendar getMinusMillis(Calendar calendar, long millis) {
        long tmp_millis = calendar.getTimeInMillis() - millis;
        calendar.setTimeInMillis(tmp_millis);
        return calendar;
    }
    public static final String I_TIME_FORMATTER = "HH:mm";
    public static String getTimeFormatter(long millis) {
        return getTimeFormatter(millis, I_TIME_FORMATTER);
    }

    public static String getTimeFormatter(long millis, String timeForamtter) {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat(I_TIME_FORMATTER);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(millis);
        String temp = sdf.format(cal.getTime());
        return temp;
    }

    public static final String I_DATE_FORMATTER = "EEEE, dd, MMM, yyyy";
    public static final String I_DATE_CALENDAR_POPUP = "dd MMM yyyy";
    public static String getDateFormatter(long millisec) {
        return getDateFormatter(millisec, I_DATE_FORMATTER);
    }
    public static String getDateFormatter(long millisec, String dateFormatter) {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormatter);
        Date date = new Date();
        date.setTime(millisec);
        String temp = sdf.format(date);
        return temp;
    }
}
