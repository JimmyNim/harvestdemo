package com.harvest.tvs.util;


import com.harvest.tvs.BuildConfig;

/**
 * Created by sh.choe on 2015-09-01.
 */
public class Log {
    public interface LogInterface {
        void v(String tag, String msg);
        void v(String tag, String msg, Throwable th);

        void d(String tag, String msg);
        void d(String tag, String msg, Throwable th);

        void i(String tag, String msg);
        void i(String tag, String msg, Throwable th);

        void w(String tag, String msg);
        void w(String tag, String msg, Throwable th);

        void e(String tag, String msg);
        void e(String tag, String msg, Throwable th);
    }

    private static final String COMMON_TAG = "Harvest";
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final boolean WITH_METHOD_INFO = true;
    private Log() {}

//    public static void v(String msg) {
//        if (DEBUG) {
//            if (LOGGER != null) {
//                LOGGER.v(getTag(), addMethodInfo(msg));
//            }
//        }
//    }

//    public static void v(String msg, Throwable th) {
//        if (DEBUG) {
//            if (LOGGER != null) {
//                LOGGER.v(getTag(), addMethodInfo(msg), th);
//            }
//        }
//    }

    public static void v(String tag, String msg) {
        if (DEBUG) {
            if (LOGGER != null) {
                LOGGER.v(""+COMMON_TAG+"_"+tag, msg);
            }
        }
    }

//    public static void d(String msg) {
//        if (DEBUG) {
//            if (LOGGER != null) {
//                LOGGER.d(getTag(), addMethodInfo(msg));
//            }
//        }
//    }

    public static void d(String tag, String msg) {
        if (DEBUG) {
            if (LOGGER != null) {
                LOGGER.d(""+COMMON_TAG+"_"+tag, msg);
            }
        }
    }

//    public static void i(String msg) {
//        if (DEBUG) {
//            if (LOGGER != null) {
//                LOGGER.i(getTag(), addMethodInfo(msg));
//            }
//        }
//    }

//    public static void i(String msg, Throwable th) {
//        if (DEBUG) {
//            if (LOGGER != null) {
//                LOGGER.i(getTag(), addMethodInfo(msg), th);
//            }
//        }
//    }

    public static void i(String tag, String msg) {
        if (DEBUG) {
            if (LOGGER != null) {
                LOGGER.i(""+COMMON_TAG+"_"+tag, msg);
            }
        }
    }

//    public static void w(String msg) {
//        if (DEBUG) {
//            if (LOGGER != null) {
//                LOGGER.w(getTag(), addMethodInfo(msg));
//            }
//        }
//    }

//    public static void w(String msg, Throwable th) {
//        if (DEBUG) {
//            if (LOGGER != null) {
//                LOGGER.w(getTag(), addMethodInfo(msg), th);
//            }
//        }
//    }

    public static void w(String tag, String msg) {
        if (DEBUG) {
            if (LOGGER != null) {
                LOGGER.w(""+COMMON_TAG+"_"+tag, msg);
            }
        }
    }

    public static void w(String tag, String msg, Throwable th) {
        if (DEBUG) {
            if (LOGGER != null) {
                LOGGER.w(""+COMMON_TAG+"_"+tag, msg, th);
            }
        }
    }

//    public static void e(String msg) {
//        if (DEBUG) {
//            if (LOGGER != null) {
//                LOGGER.e(getTag(), addMethodInfo(msg));
//            }
//        }
//    }

//    public static void e(String msg, Throwable th) {
//        if (DEBUG) {
//            if (LOGGER != null) {
//                LOGGER.e(getTag(), addMethodInfo(msg), th);
//            }
//        }
//    }

    public static void e(String tag, String msg) {
        if (DEBUG) {
            if (LOGGER != null) {
                LOGGER.e(""+COMMON_TAG+"_"+tag, msg);
            }
        }
    }

    public static void e(String tag, String msg, Throwable th) {
        if (DEBUG) {
            if (LOGGER != null) {
                LOGGER.e(""+COMMON_TAG+"_"+tag, msg, th);
            }
        }
    }

//    private static String getTag() {
//        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
////        return ste.getClassName().substring(ste.getClassName().lastIndexOf(".") + 1);
//
//        if (stackTraceElements != null) {
//            for (StackTraceElement stackTraceElement : stackTraceElements) {
//                if (stackTraceElement.isNativeMethod()) {
//                    continue;
//                }
//                if (stackTraceElement.getClassName().equals(Thread.class.getName())) {
//                    continue;
//                }
//                if (stackTraceElement.getClassName().equals(Log.class.getName())) {
//                    continue;
//                }
//                return "[" + stackTraceElement.getClassName().substring(stackTraceElement.getClassName().lastIndexOf(".") + 1); //+ "."
//                        //+ stackTraceElement.getMethodName() + "]"; // + ":" + stackTraceElement.getLineNumber() + "]";
//            }
//        }
//
//        return "[No Tag]";
//    }

    private static String getTag() {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
//        return ste.getClassName().substring(ste.getClassName().lastIndexOf(".") + 1);

        if (stackTraceElements != null) {
            for (StackTraceElement stackTraceElement : stackTraceElements) {
                if (stackTraceElement.isNativeMethod()) {
                    continue;
                }
                if (stackTraceElement.getClassName().equals(Thread.class.getName())) {
                    continue;
                }
                if (stackTraceElement.getClassName().equals(Log.class.getName())) {
                    continue;
                }
                return stackTraceElement.getClassName().substring(stackTraceElement.getClassName().lastIndexOf(".") + 1); //+ "."
                //+ stackTraceElement.getMethodName() + "]"; // + ":" + stackTraceElement.getLineNumber() + "]";
            }
        }

        return COMMON_TAG;
    }

    private static String addMethodInfo(String msg) {
        if (msg == null) {
            return null;
        }
        if (!WITH_METHOD_INFO) {
            return msg;
        }
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
//        return String.format("[%s.%s() #%d]\n=== BEGIN ===\n%s\n===  END  ===", ste.getClassName().substring(ste.getClassName().lastIndexOf(".") + 1), ste.getMethodName(), ste.getLineNumber(), msg);
//        return String.format("%s() %4d# >>>\n%s <<<", ste.getMethodName(), ste.getLineNumber(), msg);
//        return String.format("%s() %s", stackTraceElements.getMethodName(), msg);

        if (stackTraceElements != null) {
            for (StackTraceElement stackTraceElement : stackTraceElements) {
                if (stackTraceElement.isNativeMethod()) {
                    continue;
                }
                if (stackTraceElement.getClassName().equals(Thread.class.getName())) {
                    continue;
                }
                if (stackTraceElement.getClassName().equals(Log.class.getName())) {
                    continue;
                }
                return String.format("%s():%d] %s", stackTraceElement.getMethodName(), stackTraceElement.getLineNumber(), msg);
            }
        }

        return "[No Method Info]";
    }

    private static LogInterface LOGGER = new AndroidLog();

    public static void setLogger(LogInterface logger) {
        LOGGER = logger;
    }

    public static class AndroidLog implements LogInterface {

        @Override
        public void v(String tag, String msg) {
            android.util.Log.v(tag, msg);
        }

        @Override
        public void v(String tag, String msg, Throwable th) {
            android.util.Log.d(tag, msg, th);
        }

        @Override
        public void d(String tag, String msg) {
            android.util.Log.d(tag, msg);
        }

        @Override
        public void d(String tag, String msg, Throwable th) {
            android.util.Log.d(tag, msg, th);
        }

        @Override
        public void i(String tag, String msg) {
            android.util.Log.i(tag, msg);
        }

        @Override
        public void i(String tag, String msg, Throwable th) {
            android.util.Log.i(tag, msg, th);
        }

        @Override
        public void w(String tag, String msg) {
            android.util.Log.w(tag, msg);
        }

        @Override
        public void w(String tag, String msg, Throwable th) {
            android.util.Log.w(tag, msg, th);
        }

        @Override
        public void e(String tag, String msg) {
            android.util.Log.e(tag, msg);
        }

        @Override
        public void e(String tag, String msg, Throwable th) {
            android.util.Log.e(tag, msg, th);
        }
    };

    public static class JavaLog implements LogInterface {

        private void printf(String tag, String msg) {
            System.out.printf("[%s] %s\n", tag, msg);
        }

        private void printf(String tag, String msg, Throwable throwable) {
            System.out.printf("[%s] %s\n", tag, msg);
            throwable.printStackTrace();
        }

        @Override
        public void v(String tag, String msg) {
            printf(tag, msg);
        }

        @Override
        public void v(String tag, String msg, Throwable th) {
            printf(tag, msg, th);
        }

        @Override
        public void d(String tag, String msg) {
            printf(tag, msg);
        }

        @Override
        public void d(String tag, String msg, Throwable th) {
            printf(tag, msg, th);
        }

        @Override
        public void i(String tag, String msg) {
            printf(tag, msg);
        }

        @Override
        public void i(String tag, String msg, Throwable th) {
            printf(tag, msg, th);
        }

        @Override
        public void w(String tag, String msg) {
            printf(tag, msg);
        }

        @Override
        public void w(String tag, String msg, Throwable th) {
            printf(tag, msg, th);
        }

        @Override
        public void e(String tag, String msg) {
            printf(tag, msg);
        }

        @Override
        public void e(String tag, String msg, Throwable th) {
            printf(tag, msg, th);
        }
    };
}
