package com.harvest.tvs.node;


import java.io.Serializable;

public class CSNode<E> implements Serializable {

    private int index;
    private E data;
    private CSNode<E> leftChild;
    private CSNode<E> rightSibling;

    public CSNode(E data) {
        this.data = data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public E getData() {
        return data;
    }

    public void setLeftChild(CSNode<E> leftChild) {
        this.leftChild = leftChild;
    }

    public CSNode<E> getLeftChild() {
        return leftChild;
    }

    public void setRightSibling(CSNode<E> rightSibling) {
        this.rightSibling = rightSibling;
    }

    public CSNode<E> getRightSibling() {
        return rightSibling;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return data.toString();
    }
}
