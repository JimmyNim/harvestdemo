package com.harvest.tvs.node;

import com.harvest.tvs.util.Log;

import java.util.ArrayList;

/*
 * Jimmylee : child, sibling binary tree structure
 */
public class CSTree {
    private static final String tag = CSTree.class.getSimpleName();

//    public static CSNode<Node> root;

    // add child to parent node.
    public static <E> void add(CSNode<E> parent, CSNode<E> child) {
        // 부모 노드의 자식 노드가 없다면
        if(parent.getLeftChild() == null)
            parent.setLeftChild(child);
            // 부모 노드의 자식 노드가 있다면
        else {
            // 자식 노드의 형제로 노드로 추가
            CSNode<E> temp = parent.getLeftChild();
            while(temp.getRightSibling() != null)
                temp = temp.getRightSibling();

            temp.setRightSibling(child);
        }
    }

    public static <Node> ArrayList<CSNode<Node>> getChildNodes(CSNode<Node> node) {
        ArrayList<CSNode<Node>> resultList = new ArrayList<>();
        if(node==null) return resultList;

        StringBuffer sb = new StringBuffer();
        sb.append("Result : [" + node.getData() + "] = ");
        CSNode<Node> tempParent = node;
        CSNode<Node> tempChild = tempParent.getLeftChild();
        while(tempChild != null) {
            // 데이터를 출력하고 형제노드로 이동한다.
//                    sb.append(tempChild.getData() + " ");
            Log.d(tag, String.format("[%s][%s]", ""+tempChild.getData(), ""+tempChild.getLeftChild()));
            resultList.add(tempChild);
            tempChild = tempChild.getRightSibling();
        }

        if(resultList.size()!=0) {
            for(int i=0; i<resultList.size(); i++) {
                sb.append(""+resultList.get(i).getData());
                if(i!=resultList.size()-1) {
                    sb.append(" | ");
                }
            }
        } else {
            sb.append("Empty");
        }
        Log.d(tag, "" + sb.toString());
        return resultList;
    }

    // debug print CSNode Level
    public static <E> void printLevel(CSNode<E> node, int level) {
        int depth = 0;
        CSNode<E> tempChild = node;
        CSNode<E> tempParent = node;

        while(depth <= level) {
            if(depth == level) {
                // if the node exist in current level해당 레벨의 노드가 존재한다면
                StringBuilder sb = new StringBuilder();

                while(tempChild != null) {
                    // 데이터를 출력하고 형제노드로 이동한다.
                    sb.append(tempChild.getData() + " ");
                    tempChild = tempChild.getRightSibling();
                }
                if(sb.length()!=0) Log.d(tag, sb.toString());

                // 부모 노드의 형제노드가 존재한다면
                // 그 노드의 자식 노드들도 출력해줘야 한다.
                if(tempParent.getRightSibling() != null) {
                    tempParent = tempParent.getRightSibling();
                    tempChild = tempParent.getLeftChild();
                } else
                    break;
            } else {
                // if depth and level are incorrect,
                // save parent node then add depth for child node.
                tempParent = tempChild;
                tempChild = tempChild.getLeftChild();
                depth++;
            }
        }
    }

    // print all tree
    public static <E> void printTree(CSNode<E> node, int depth) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < depth; i++)
            sb.append("+");

        // print data.
        Log.d(tag, ""+sb.toString() + node.getData());

        if(node.getLeftChild() != null)
            printTree(node.getLeftChild(), depth + 1);

        // if the sibling exist
        if(node.getRightSibling() != null)
            printTree(node.getRightSibling(), depth);
    }

}
